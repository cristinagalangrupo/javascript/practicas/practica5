/* Declaro las variables */
var correctas = ["c", "a", "c", "a", "c", "b", "a", "c", "d", "b"];
var preguntas = [".b1", ".b2", ".b3", ".b4", ".b5", ".b6", ".b7", ".b8", ".b9", ".b10"];// guardo las clases de las preguntas para leer las respuestas del usuario
var respuesta = [];

var aciertos = 0;
var vacias = 0;
var fallos = 0;
var cont=0;
/* Fin declaración de variables */

/* ************************** Temporizador ************************** */

function temporizador() {
    setTimeout(function(){ 
    	alert("Fin del tiempo"); 
    }, 60000);
}

/* fin del temporizador */

/* ************************** Función envía **************************  */

function envia() {
	var cuenta=0;
    //leer(preguntas);
    leerm();
    corregir(correctas,respuesta);
    var puntuacion=(aciertos*1 - fallos*0.5); // calcula la puntuación: acierto +1 fallo -0.5
   
    document.querySelector(".sol").innerHTML="Contestadas: "+ (aciertos+fallos)+"<br>" +"Aciertos: " + aciertos +"<br>"+ " Fallos: " + fallos+"<br>" + " Preguntas en blanco: " + vacias+"<br>" + "Puntuación: "+ puntuacion
    document.querySelector(".mostrarSoluciones").style.opacity=1; // muestro la ventana
    cuenta++;
    console.log(cuenta);

    if (cuenta>1) {
    	respuesta=[];
    };
}

/* Fin función envía */


/* *** Función leer *** Así se me ocurrió al principio, pero voy a usar la función leer mejorada */

function leer(preg) {
	var contestado=0;
    for (i = 0; i < preg.length; i++) {
    	var cajas = document.querySelectorAll(preg[i]);
        for (var j = 0; j < cajas.length; j++) {
            if (cajas[j].checked) {
                respuesta.push(cajas[j].value);
                contestado=1;
            }
        }
        if(contestado==0){
        	respuesta.push("blanco");
        }
        contestado=0;
    }
    console.log(respuesta);
}
/* Fin función leer */


/* ************************** Función leer mejorada con un contador y concatenando ".b"+contador ************************** */
// puedo prescindir del array con las clases
function leerm() {
	var contestado=0;
	
    for (i = 0; i < 10; i++) {
    	var cajas = document.querySelectorAll(".b"+(i+1)); // le sumo 1 porque las id empiezan en 1 y la i que es el contador en 0
        for (var j = 0; j < cajas.length; j++) {
            if (cajas[j].checked) {
                respuesta.push(cajas[j].value);
                contestado=1;
            }
        }
        if(contestado==0){
        	respuesta.push("blanco");
        }
        contestado=0;
    }
    console.log(respuesta);
}

/* Fin función leer mejorada */

/* ************************** Función que corrige las respuestas **************************  */ 
function corregir(corr,resp){
	for(i=0;i<corr.length;i++){
		if (corr[i]==resp[i]) {
			aciertos++;
		} else if (resp[i]=="blanco") {
			vacias++;
		}
		else{
			fallos++;
		}
	}
}
/* Fin función corregir */ 
